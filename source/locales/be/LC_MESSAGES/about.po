# Belarusian translation of the Debian Release Notes
# Copyright (C) 2009 Hleb Rubanau
# This file is distributed under the same license as the Debian Release
# Notes.
# Hleb Rubanau <g.rubanau@gmail.com>, 2009
msgid ""
msgstr ""
"Project-Id-Version:  \n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-11-05 13:18+0100\n"
"PO-Revision-Date: 2009-03-28 16:00+0200\n"
"Last-Translator: Hleb Rubanau <g.rubanau@gmail.com>\n"
"Language: be\n"
"Language-Team: Belarusian Debian <debian-l10n-"
"belarusian@lists.debian.org>\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../about.rst:4
msgid "Introduction"
msgstr "Уводзіны"

#: ../about.rst:6
msgid ""
"This document informs users of the Debian distribution about major "
"changes in version |RELEASE| (codenamed |RELEASENAME|)."
msgstr ""
"Гэты дакумент утрымлівае інфармацыю для карыстальнікаў дыстрыбутыву "
"Debian аб прынцыповых зменах у версіі |RELEASE| (кодавая назва "
"|RELEASENAME|)."

#: ../about.rst:9
msgid ""
"The release notes provide information on how to upgrade safely from "
"release |OLDRELEASE| (codenamed |OLDRELEASENAME|) to the current release "
"and inform users of known potential issues they could encounter in that "
"process."
msgstr ""
"У звестках аб выпуску даводзіцца, як карэктна абнавіць сістэму з "
"папярэдняй версіі |OLDRELEASE| (кодавая назва |OLDRELEASENAME|) да новага"
" выпуску. Таксама апісваюцца вядомыя патэнцыйныя праблемы, якія варта "
"прыняць да ўвагі падчас абнаўлення."

#: ../about.rst:14
msgid ""
"You can get the most recent version of this document from "
"|URL-R-N-STABLE|."
msgstr "Найноўшая версія гэтага дакумента публікуецца на |URL-R-N-STABLE|."

# REVIEW: распаўсюджанасць
#: ../about.rst:19
msgid ""
"Note that it is impossible to list every known issue and that therefore a"
" selection has been made based on a combination of the expected "
"prevalence and impact of issues."
msgstr ""
"Майце на ўвазе, што апісаць абсалютна ўсе вядомыя праблемы немагчыма, "
"таму сітуацыі адбіраліся ў залежнасці ад меркаванай распаўсюджанасці іх "
"узнікнення і цяжару магчымых наступстваў. "

#: ../about.rst:23
msgid ""
"Please note that we only support and document upgrading from the previous"
" release of Debian (in this case, the upgrade from |OLDRELEASENAME|). If "
"you need to upgrade from older releases, we suggest you read previous "
"editions of the release notes and upgrade to |OLDRELEASENAME| first."
msgstr ""
"Калі ласка, звярніце ўвагу, што мы забяспечваем падтрымку і апісанне "
"толькі працэсу абнаўлення з папярэдняга выпуску Debian (у гэтым выпадку "
"абнаўлення з версіі |OLDRELEASENAME|). Каб зрабіць абнаўленне з больш "
"старых версій, варта спачатку звярнуцца да адпаведных звестак аб выпусках"
" і абнавіць сістэму да |OLDRELEASENAME|."

#: ../about.rst:32
msgid "Reporting bugs on this document"
msgstr "Як паведаміць аб памылках у гэтым дакуменце"

#: ../about.rst:34
msgid ""
"We have attempted to test all the different upgrade steps described in "
"this document and to anticipate all the possible issues our users might "
"encounter."
msgstr ""
"Мы паспрабавалі праверыць усе магчымыя крокі па абнаўленні сістэмы, "
"апісаныя ў гэтым дакуменце, і прадбачыць усе праблемы, з якімі могуць "
"сутыкнуцца нашы карыстальнікі."

# REVIEW: translators add-on, email syntax.
# | msgid ""
# | "Nevertheless, if you think you have found a bug (incorrect information or
# "
# | "information that is missing)  in this documentation, please file a bug in
# | "the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
# | "<systemitem role=\"package\">release-notes</systemitem> package."
#: ../about.rst:38
#, fuzzy
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or"
" information that is missing) in this documentation, please file a bug in"
" the `bug tracking system <https://bugs.debian.org/>`__ against the "
"**release-notes** package. You might first want to review the `existing "
"bug reports <https://bugs.debian.org/release-notes>`__ in case the issue "
"you've found has already been reported. Feel free to add additional "
"information to existing bug reports if you can contribute content for "
"this document."
msgstr ""
"Тым не менш, калі Вам здаецца, што ў дакуменце знойдзеная памылка "
"(непраўдзівыя звесткі альбо адсутнасць патрэбнай інфармацыі), ласкава "
"просім даслаць паведамленне пра памылку пакета **release-notes**, "
"скарыстаўшыся `сістэмай кантролю памылак <https://bugs.debian.org/>`__. "
"ЗАЎВАГА ПЕРАКЛАДЧЫКАЎ: калі ласка, ужывайце пры гэтым англійскую мову. "
"Звесткі аб памылках у беларускім перакладзе альбо беларускамоўныя "
"паведамленні дасылайце на адрас электроннай рассылкі "
"debian-l10-belarusian@lists.debian.org."

#: ../about.rst:47
msgid ""
"We appreciate, and encourage, reports providing patches to the document's"
" sources. You will find more information describing how to obtain the "
"sources of this document in `Sources for this document <#sources>`__."
msgstr ""

#: ../about.rst:55
msgid "Contributing upgrade reports"
msgstr "Як падзяліцца уласным досведам"

#: ../about.rst:57
msgid ""
"We welcome any information from users related to upgrades from "
"|OLDRELEASENAME| to |RELEASENAME|. If you are willing to share "
"information please file a bug in the `bug tracking system "
"<https://bugs.debian.org/>`__ against the **upgrade-reports** package "
"with your results. We request that you compress any attachments that are "
"included (using ``gzip``)."
msgstr ""
"Мы запрашаем усіх карыстальнікаў дзяліцца карыснай інфармацыяй адносна "
"працэсу абнаўлення з |OLDRELEASENAME| да |RELEASENAME|. Калі Вы жадаеце "
"далучыцца да абмену досведам, дашліце сваю справаздачу праз `сістэму "
"кантролю памылак <https://bugs.debian.org/>`__ ў фармаце паведамлення аб "
"памылцы пакета **upgrade-reports**. Усе дададзеныя файлы трэба сціскаць з"
" дапамогай праграмы ``gzip``. Заўвага перакладчыкаў: згаданыя справаздачы"
" варта запаўняць па англійску альбо звяртацца ў электронную рассылку "
"debian-l10-belarusian@lists.debian.org, каб абмеркаваць магчымасць "
"дапамогі з перакладам."

#: ../about.rst:63
msgid ""
"Please include the following information when submitting your upgrade "
"report:"
msgstr ""
"Справаздача аб абнаўленні абавязкова мусіць утрымліваць наступныя "
"звесткі: "

#: ../about.rst:66
#, fuzzy
msgid ""
"The status of your package database before and after the upgrade: "
"**dpkg**'s status database available at ``/var/lib/dpkg/status`` and "
"**apt**'s package state information, available at "
"``/var/lib/apt/extended_states``. You should have made a backup before "
"the upgrade as described at :ref:`data-backup`, but you can also find "
"backups of ``/var/lib/dpkg/status`` in ``/var/backups``."
msgstr ""
"Стан базы пакетаў да пачатку і пасля сканчэння абнаўлення: база статусаў "
"праграмы **dpkg** месціцца ў каталогу ``/var/lib/dpkg/status``, а база "
"статусаў праграмы **aptitude** -- у каталогу "
"``/var/lib/aptitude/pkgstates``. Напярэдадні абнаўлення варта зрабіць "
"рэзервовую копію ў адпаведнасці з парадамі главы :ref:`data-backup`, але "
"можна таксама выкарыстаць копію адпаведнай інфармацыі, якая аўтаматычна "
"захоўваецца ў каталогу ``var/backups``."

#: ../about.rst:74
msgid ""
"Session logs created using ``script``, as described in :ref:`record-"
"session`."
msgstr ""
"Пратаколы сесіі, атрыманыя з дапамогай праграмы ``script`` (гл. :ref"
":`record-session`)."

# | msgid ""
# | "Your <systemitem role=\"package\">apt</systemitem> logs, available at "
# | "<filename>/var/log/apt/term.log</filename> or your <command>aptitude</"
# | "command> logs, available at <filename>/var/log/aptitude</filename>."
#: ../about.rst:77
#, fuzzy
msgid ""
"Your ``apt`` logs, available at ``/var/log/apt/term.log``, or your "
"``aptitude`` logs, available at ``/var/log/aptitude``."
msgstr ""
"Пратаколы сістэмы ``apt``, якія знаходзяцца ў файле "
"``/var/log/apt/term.log``, альбо пратаколы праграмы ``aptitude``, якія "
"месцяцца ў каталогу ``/var/log/aptitude``."

#: ../about.rst:82
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug "
"report as the information will be published in a public database."
msgstr ""
"Перад тым, як дададаць гэтыя пратаколы да справаздачы, варта перагледзець"
" іх і выдаліць прыватныя звесткі з улікам таго, што дасланая інфармацыя "
"будзе апублікаваная ў адкрытым доступе."

#: ../about.rst:89
msgid "Sources for this document"
msgstr "Зыходныя файлы гэтага дакументу"

#: ../about.rst:91
msgid ""
"The source of this document is in reStructuredText format, using the "
"sphinx converter. The HTML version is generated using *sphinx-build -b "
"html*. The PDF version is generated using *sphinx-build -b latex*. "
"Sources for the Release Notes are available in the Git repository of the "
"*Debian Documentation Project*. You can use the `web interface "
"<https://salsa.debian.org/ddp-team/release-notes/>`__ to access its files"
" individually through the web and see their changes. For more information"
" on how to access Git please consult the `Debian Documentation Project "
"VCS information pages <https://www.debian.org/doc/vcs>`__."
msgstr ""

