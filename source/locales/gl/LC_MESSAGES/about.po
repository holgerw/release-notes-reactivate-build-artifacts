# Translation of about.po to Galician
# Copyright (C) 2021 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Pablo <parodper@gmail.com>, 2021.
#
# Traductores:
# Para esta traducción usei (Pablo) o dicionario do Proxecto Trasno
# (http://termos.trasno.gal/) e o DiGaTIC (http://www.digatic.org/gl)
msgid ""
msgstr ""
"Project-Id-Version: release-notes 11\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-11-05 13:18+0100\n"
"PO-Revision-Date: 2021-06-10 19:22+0200\n"
"Last-Translator: Pablo <parodper@gmail.com>\n"
"Language: gl\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../about.rst:4
msgid "Introduction"
msgstr "Introdución"

#: ../about.rst:6
msgid ""
"This document informs users of the Debian distribution about major "
"changes in version |RELEASE| (codenamed |RELEASENAME|)."
msgstr ""
"O obxectivo deste documento é informar aos usuarios da distribución "
"Debian sobre os principais cambios na versión |RELEASE| (alcumada "
"|RELEASENAME|)."

#: ../about.rst:9
msgid ""
"The release notes provide information on how to upgrade safely from "
"release |OLDRELEASE| (codenamed |OLDRELEASENAME|) to the current release "
"and inform users of known potential issues they could encounter in that "
"process."
msgstr ""
"As notas da versión conteñen máis información sobre como se pode "
"actualizar de forma segura dende a versión |OLDRELEASE| (alcumada "
"|OLDRELEASENAME|) á versión actual e informan aos usuarios dos posibles "
"problemas que se sabe poden ocorrer."

#: ../about.rst:14
msgid ""
"You can get the most recent version of this document from "
"|URL-R-N-STABLE|."
msgstr "Podes obter a última versión deste documento en |URL-R-N-STABLE|."

#: ../about.rst:19
msgid ""
"Note that it is impossible to list every known issue and that therefore a"
" selection has been made based on a combination of the expected "
"prevalence and impact of issues."
msgstr ""
"Teña en conta que é imposible amosar todos os problemas que se coñecen, "
"polo que foi necesario facer unha selección baseándose na probabilidade "
"de que ocorran e o seu impacto."

#: ../about.rst:23
msgid ""
"Please note that we only support and document upgrading from the previous"
" release of Debian (in this case, the upgrade from |OLDRELEASENAME|). If "
"you need to upgrade from older releases, we suggest you read previous "
"editions of the release notes and upgrade to |OLDRELEASENAME| first."
msgstr ""
"Lembre que só lle damos asistencia técnica para actualizar dende a "
"versión de Debian anterior (neste caso, actualizar dende "
"|OLDRELEASENAME|).  Se necesitas actualizar dende versións anteriores, "
"suxerímoslle que lea as edicións anteriores das notas da versión e "
"actualice antes a |OLDRELEASENAME|."

#: ../about.rst:32
msgid "Reporting bugs on this document"
msgstr "Avise de erros neste documentos"

#: ../about.rst:34
msgid ""
"We have attempted to test all the different upgrade steps described in "
"this document and to anticipate all the possible issues our users might "
"encounter."
msgstr ""
"Probamos todos os diferentes pasos descritos neste documento para "
"realizar a actualización e intentamos anticiparnos a todos os problemas "
"que se poidan atopar os nosos usuarios."

#: ../about.rst:38
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or"
" information that is missing) in this documentation, please file a bug in"
" the `bug tracking system <https://bugs.debian.org/>`__ against the "
"**release-notes** package. You might first want to review the `existing "
"bug reports <https://bugs.debian.org/release-notes>`__ in case the issue "
"you've found has already been reported. Feel free to add additional "
"information to existing bug reports if you can contribute content for "
"this document."
msgstr ""
"De todas formas se vostede cre que atopou un fallo (información errónea "
"ou ausente)  nesta documentación, por favor abra unha petición no "
"`sistema de seguimento de fallos <https://bugs.debian.org/>`__ sobre o "
"paquete **release-notes**. Revise os `informes de fallos anteriores "
"<https://bugs.debian.org/release-notes>`__ en caso de que alguén xa "
"informara sobre o problema que atopou. Engada sen medo nova información "
"aos informes xa existentes se pode contribuír con contido para este "
"documento."

#: ../about.rst:47
#, fuzzy
msgid ""
"We appreciate, and encourage, reports providing patches to the document's"
" sources. You will find more information describing how to obtain the "
"sources of this document in `Sources for this document <#sources>`__."
msgstr ""
"Agradecémoslle, e animámoslle a, que engada parches nas fontes do "
"documento xunto cos informes. Pode obter máis información de como obter "
"as fontes deste documento en `Sources for this document <#sources>`__."

#: ../about.rst:55
msgid "Contributing upgrade reports"
msgstr "Colaborando con informes de actualización"

#: ../about.rst:57
msgid ""
"We welcome any information from users related to upgrades from "
"|OLDRELEASENAME| to |RELEASENAME|. If you are willing to share "
"information please file a bug in the `bug tracking system "
"<https://bugs.debian.org/>`__ against the **upgrade-reports** package "
"with your results. We request that you compress any attachments that are "
"included (using ``gzip``)."
msgstr ""
"Toda colaboración por parte dos usuarios relacionada coas actualizacións "
"dende |OLDRELEASENAME| a |RELEASENAME| é benvida.  Se esta disposto a "
"compartir información por favor abra unha petición no `sistema de "
"seguimento de fallos <https://bugs.debian.org/>`__ sobre o paquete "
"**upgrade-reports** coas súas achegas.  Pedímoslle que comprima todos os "
"ficheiros que engada (usando ``gzip``)."

#: ../about.rst:63
msgid ""
"Please include the following information when submitting your upgrade "
"report:"
msgstr ""
"Por favor inclúa a seguinte información cando envíe o seu informe de "
"actualización:"

#: ../about.rst:66
msgid ""
"The status of your package database before and after the upgrade: "
"**dpkg**'s status database available at ``/var/lib/dpkg/status`` and "
"**apt**'s package state information, available at "
"``/var/lib/apt/extended_states``. You should have made a backup before "
"the upgrade as described at :ref:`data-backup`, but you can also find "
"backups of ``/var/lib/dpkg/status`` in ``/var/backups``."
msgstr ""
"O estado da súa base de datos de paquetes antes e despois da "
"actualización: O estado da base de datos de **dpkg** pódese obter dende "
"``/var/lib/dpkg/status``; tamén engada o estado dos paquetes de **apt**, "
"indicado en ``/var/lib/apt/extended_states``.  Debería ter feito unha "
"copia de seguridade antes de actualizar, tal como se indica en :ref"
":`data-backup`, pero tamén pode atopar copias de seguridade de "
"``/var/lib/dpkg/status`` en ``/var/backups``."

#: ../about.rst:74
msgid ""
"Session logs created using ``script``, as described in :ref:`record-"
"session`."
msgstr ""
"Os rexistros da sesión creados con ``script``, tal como se indica en :ref"
":`record-session`."

#: ../about.rst:77
msgid ""
"Your ``apt`` logs, available at ``/var/log/apt/term.log``, or your "
"``aptitude`` logs, available at ``/var/log/aptitude``."
msgstr ""
"Os seus rexistros de ``apt``, dispoñibles en ``/var/log/apt/term.log``; "
"ou os rexistros de ``aptitude``, dispoñibles en ``/var/log/aptitude``."

#: ../about.rst:82
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug "
"report as the information will be published in a public database."
msgstr ""
"Debería revisar con calma e eliminar calquera información persoal e/ou "
"confidencial dos rexistros antes de incluílos no seu informe de fallos, "
"posto que a información publicarase nunha base de datos pública."

#: ../about.rst:89
msgid "Sources for this document"
msgstr "Fontes deste documento"

#: ../about.rst:91
msgid ""
"The source of this document is in reStructuredText format, using the "
"sphinx converter. The HTML version is generated using *sphinx-build -b "
"html*. The PDF version is generated using *sphinx-build -b latex*. "
"Sources for the Release Notes are available in the Git repository of the "
"*Debian Documentation Project*. You can use the `web interface "
"<https://salsa.debian.org/ddp-team/release-notes/>`__ to access its files"
" individually through the web and see their changes. For more information"
" on how to access Git please consult the `Debian Documentation Project "
"VCS information pages <https://www.debian.org/doc/vcs>`__."
msgstr ""
"As fontes deste documento están en formato DocBook "
"XML<indexterm><primary>DocBook XML</primary></indexterm>. A versión HTML "
"é xerada con <systemitem role=\"package\">docbook-xsl</systemitem> e "
"<systemitem role=\"package\">xsltproc</systemitem>. A versión en PDF "
"xérase usando <systemitem role=\"package\">dblatex</systemitem> ou "
"<systemitem role=\"package\">xmlroff</systemitem>. As fontes das Notas de"
" Versión atópanse no repositorio Git do *Proxecto de Documentación "
"Debian*.  Pode usar a `interface na rede <https://salsa.debian.org/ddp-"
"team/release-notes/>`__ para acceder aos ficheiros individuais a través "
"da rede e ver os seus cambios.  Para máis información sobre como acceder "
"a Git consulte as `páxinas de información sobre SCV do Proxecto de "
"Documentación Debian <https://www.debian.org/doc/vcs>`__."

