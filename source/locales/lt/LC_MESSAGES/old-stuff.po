# SOME DESCRIPTIVE TITLE
# Copyright (C) 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the release-notes
# package.
# Kęstutis Biliūnas <kebil@kaunas.init.lt>, 2009.
# , fuzzy
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-22 20:43+0200\n"
"PO-Revision-Date: 2009-02-12 09:45+0200\n"
"Last-Translator: Kęstutis Biliūnas <kebil@kaunas.init.lt>\n"
"Language: lt\n"
"Language-Team: Lithuanian <komp_lt@konferencijos.lt>\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"(n%100<10 || n%100>=20) ? 1 : 2);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

# | msgid "Managing your |OLDRELEASENAME| system"
#: ../old-stuff.rst:4
#, fuzzy
msgid "Managing your |OLDRELEASENAME| system before the upgrade"
msgstr "Sistemos |OLDRELEASENAME| tvarkymas"

#: ../old-stuff.rst:6
msgid ""
"This appendix contains information on how to make sure you can install or"
" upgrade |OLDRELEASENAME| packages before you upgrade to |RELEASENAME|."
msgstr ""
"Šiame priede pateikiama informacija apie tai, kaip įsitikinti, kad galite"
" įdiegti ar atnaujinti |OLDRELEASENAME| paketus prieš atnaujinant iki "
"|RELEASENAME|."

#: ../old-stuff.rst:12
msgid "Upgrading your |OLDRELEASENAME| system"
msgstr "Sistemos |OLDRELEASENAME| atnaujinimas"

#: ../old-stuff.rst:14
#, fuzzy
msgid ""
"Basically this is no different from any other upgrade of |OLDRELEASENAME|"
" you've been doing. The only difference is that you first need to make "
"sure your package list still contains references to |OLDRELEASENAME| as "
"explained in `Checking your APT source-list files <#old-sources>`__."
msgstr ""
"Iš esmės šis atnaujinimas niekuo nesiskiria nuo kitų |OLDRELEASENAME| "
"atnaujinimų, kuriuos jau darėte. Vienintelis skirtumas yra tai, kad "
"pirmiausia turite įsitikinti, kad paketų sąrašas vis dar turi nuorodą į "
"|OLDRELEASENAME| laidą, kaip tai paaiškinta <#old-sources>`__."

#: ../old-stuff.rst:19
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically "
"be upgraded to the latest |OLDRELEASENAME| point release."
msgstr ""
"Jei atnaujinate iš Debian'o serverio-veidrodžio, tai automatiškai bus "
"atliktas atnaujinimas paskutiniosios |OLDRELEASENAME| laidos."

# | msgid "Checking your sources list"
#: ../old-stuff.rst:25
#, fuzzy
msgid "Checking your APT source-list files"
msgstr "APT šaltinių sąrašo tikrinimas"

#: ../old-stuff.rst:27
#, fuzzy
msgid ""
"If any of the lines in your APT source-list files (see :url-man-"
"stable:`sources.list(5)`) contain references to \"stable\", this is "
"effectively pointing to |RELEASENAME| already. This might not be what you"
" want if you are not yet ready for the upgrade. If you have already run "
"``apt update``, you can still get back without problems by following the "
"procedure below."
msgstr ""
"Jei kokia nors eilutė faile ``/etc/apt/sources.list`` nurodo į 'stable' "
"distributyvą, Jūs jau \"naudojate\" |RELEASENAME|. Jei net jau naudojote "
"komandą ``apt-get update``, galite dar be problemų grįžti atgal, "
"taikydami žemiau aprašytą procedūrą."

#: ../old-stuff.rst:34
msgid ""
"If you have also already installed packages from |RELEASENAME|, there "
"probably is not much point in installing packages from |OLDRELEASENAME| "
"anymore. In that case you will have to decide for yourself whether you "
"want to continue or not. It is possible to downgrade packages, but that "
"is not covered here."
msgstr ""
"Jei jau įdiegėte paketus iš |RELEASENAME|, tai nebėra daug prasmės "
"įdiegti paketus iš |OLDRELEASENAME|. Tokiu atveju turite nuspręsti patys,"
" ar norite tęsti, ar ne. Grįžti prie senų paketų naudojimo įmanoma, bet "
"čia apie tai nekalbėsime."

#: ../old-stuff.rst:40
#, fuzzy
msgid ""
"As root, open the relevant APT source-list file (such as "
"``/etc/apt/sources.list``) with your favorite editor, and check all lines"
" beginning with"
msgstr ""
"Atverkite failą ``/etc/apt/sources.list`` savo mėgiamu redaktoriumi "
"(turėdami administratoriaus ``root`` privilegijas) ir raskite visas "
"eilutes prasidedančias kaip"

#: ../old-stuff.rst:44
msgid "``deb http:``"
msgstr ""

#: ../old-stuff.rst:45
msgid "``deb https:``"
msgstr ""

#: ../old-stuff.rst:46
msgid "``deb tor+http:``"
msgstr ""

#: ../old-stuff.rst:47
msgid "``deb tor+https:``"
msgstr ""

#: ../old-stuff.rst:48
msgid "``URIs: http:``"
msgstr ""

#: ../old-stuff.rst:49
msgid "``URIs: https:``"
msgstr ""

#: ../old-stuff.rst:50
msgid "``URIs: tor+http:``"
msgstr ""

#: ../old-stuff.rst:51
msgid "``URIs: tor+https:``"
msgstr ""

#: ../old-stuff.rst:53
#, fuzzy
msgid ""
"for a reference to \"stable\". If you find any, change \"stable\" to "
"\"|OLDRELEASENAME|\"."
msgstr ""
"ir turinčias žodį \"stable\". Radę tokias eikutes, pakeiskite žodį "
"\"stable\" į žodį \"|OLDRELEASENAME|\"."

# | msgid ""
# | "If you have any lines starting with <literal>deb file:</literal>, you "
# | "will have to check for yourself if the location they refer to contains an
# "
# | "|OLDRELEASENAME| or a |RELEASENAME| archive."
#: ../old-stuff.rst:55
#, fuzzy
msgid ""
"If you have any lines starting with ``deb file:`` or ``URIs: file:``, you"
" will have to check for yourself if the location they refer to contains a"
" |OLDRELEASENAME| or |RELEASENAME| archive."
msgstr ""
"Jei radote eilučių, prasidedančių ``deb file:``, turite pats patikrinti, "
"kokie paketai saugomi nurodytame kataloge: ar tai |OLDRELEASENAME| ar "
"|RELEASENAME| archyvas."

# | msgid ""
# | "Do not change any lines that begin with <literal>deb cdrom:</literal>.  "
# | "Doing so would invalidate the line and you would have to run
# <command>apt-"
# | "cdrom</command> again.  Do not be alarmed if a 'cdrom' source line refers
# "
# | "to <quote><literal>unstable</literal></quote>.  Although confusing, this
# | "is normal."
#: ../old-stuff.rst:61
#, fuzzy
msgid ""
"Do not change any lines that begin with ``deb cdrom:`` or ``URIs: "
"cdrom:``. Doing so would invalidate the line and you would have to run "
"``apt-cdrom`` again. Do not be alarmed if a ``cdrom:`` source line refers"
" to \"unstable\". Although confusing, this is normal."
msgstr ""
"Nekeiskite eilutės, prasidedančios ``deb cdrom:``. Jei taip padarysite "
"gali tekti iš naujo paleisti vykdyti programą ``apt-cdrom``. "
"Nesijaudinkite jei 'cdrom' eilutėje yra nuoroda į \"unstable\". Nors tai "
"ir keista, tai yra normalu."

#: ../old-stuff.rst:67
msgid "If you've made any changes, save the file and execute"
msgstr ""
"Jei atlikote kokius nors pakeitimus, išsaugokite failą ir vykdykite "
"komandą"

#: ../old-stuff.rst:73
msgid "to refresh the package list."
msgstr "tam, kad būtų atnaujintas paketų sąrašas."

#: ../old-stuff.rst:78
msgid "Performing the upgrade to latest |OLDRELEASENAME| release"
msgstr ""

#: ../old-stuff.rst:80
msgid ""
"To upgrade all packages to the state of the latest point release for "
"|OLDRELEASENAME|, do"
msgstr ""

#: ../old-stuff.rst:90
msgid "Removing obsolete configuration files"
msgstr ""

#: ../old-stuff.rst:92
msgid ""
"Before upgrading your system to |RELEASENAME|, it is recommended to "
"remove old configuration files (such as ``*.dpkg-{new,old}`` files under "
"``/etc``) from the system."
msgstr ""

